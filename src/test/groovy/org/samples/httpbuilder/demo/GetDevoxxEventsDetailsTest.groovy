package org.samples.httpbuilder.demo

import groovyx.net.http.HTTPBuilder
import groovyx.net.http.HttpResponseException
import org.junit.Before
import org.junit.Test

import static groovyx.net.http.ContentType.JSON
import static groovyx.net.http.Method.GET

class GetDevoxxEventsDetailsTest {

    public static final String USER_AGENT = "Mozilla/5.0 Firefox/3.0.4"

    private HTTPBuilder http;

    @Before
    public void setUp() {
        http = new HTTPBuilder('http://cfp.devoxx.be')
    }

    @Test
    def void getAllConferencesDetails_UserAgentNotSpecified_ShouldReturnForbiddenStatus() {
        try {
            http.get( path : '/api/conferences')
            assert false : "Exception should be thrown"
        }
        catch (HttpResponseException ex) {
            assert ex.response.status == 403
        }
    }

    @Test
    def void getAllConferencesDetails_UserAgentSpecified_ShouldReturnDetailsForAllConferences() {
        http.request(GET, JSON ) { req ->
            uri.path = '/api/conferences'
            headers.'User-Agent' = USER_AGENT
            headers.Accept = 'application/json'

            response.success = { resp, json ->
                assert resp.statusLine.statusCode == 200

                assert json.content == "All conferences"
                assert json.links.size() > 0
            }
        }
    }

    @Test
    def void getDevoxxBe15Details_UserAgentSpecified_ShouldReturnDetailsForDevoxxBe15() {
        http.request(GET, JSON ) { req ->
            uri.path = '/api/conferences/DV15'
            headers.'User-Agent' = USER_AGENT
            headers.Accept = 'application/json'

            response.success = { resp, json ->
                assert resp.statusLine.statusCode == 200

                assert json.label == "Devoxx Belgium 2015, 9th-13th November"
                assert json.eventCode == "DV15"
                assert json.links.size() > 0
            }
        }
    }

}
