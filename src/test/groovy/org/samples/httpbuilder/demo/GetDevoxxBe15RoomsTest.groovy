package org.samples.httpbuilder.demo

import groovyx.net.http.HTTPBuilder
import org.junit.Test

import static groovyx.net.http.ContentType.JSON
import static groovyx.net.http.Method.GET

class GetDevoxxBe15RoomsTest {

    public static final String USER_AGENT = "Mozilla/5.0 Firefox/3.0.4"
    public static final String SETUP_1 = "theatre"
    public static final String SETUP_2 = "classroom"
    public static final String SETUP_3 = "special"

    def http = new HTTPBuilder('http://cfp.devoxx.be')

    @Test
    def void getAllRooms_ShouldReturnAllRoomsForDevoxxBe15() {
        http.request(GET, JSON) { req ->
            uri.path = '/api/conferences/DV15/rooms'
            headers.'User-Agent' = USER_AGENT
            headers.Accept = 'application/json'

            response.success = { resp, json ->
                assert resp.statusLine.statusCode == 200

                assert json.content == "All rooms"
                assert json.rooms instanceof List
                assert json.rooms.size() > 0
                assert json.rooms.each {
                    it.name.length() > 0
                    it.setup == SETUP_1 || it.setup == SETUP_2 || it.setup == SETUP_3
                    it.capacity > 50
                }
            }
        }
    }

}
