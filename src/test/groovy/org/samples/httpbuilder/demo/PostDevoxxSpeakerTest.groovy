package org.samples.httpbuilder.demo

import groovy.util.slurpersupport.GPathResult
import groovyx.net.http.RESTClient
import org.junit.Before
import org.junit.Test

class PostDevoxxSpeakerTest {

    public static final String TALK_TITLE = 'Updates to the Java API for JSON Processing for Java EE 8'

    /**
     * RESTClient is an extension of HTTPBuilder. It uses automatic content-type parsing and encoding
     * which makes working with XML and JSON extremely easy.
     */
    private RESTClient http;

    @Before
    public void setUp() {
        http = new RESTClient("http://restmirror.appspot.com/")
    }

    @Test
    public void postSpeaker_JsonSpeakerProvided_ShouldMirrorPostedSpeaker() {
        def response = http.post(
                path:'/',
                contentType:'application/json',
                body: [
                        firstName: 'Alex',
                        lastName: 'Soto',
                        company: 'CloudBees',
                        twitter: '@alexsotob',
                        talks: [
                                title: TALK_TITLE,
                                track: 'Server Side Java'
                        ]
                ]
        )

        assert response.status == 201
        println "Success: ${response.statusLine}"

        assert response.data instanceof Map
        assert response.data.firstName == 'Alex'
        assert response.data.lastName == 'Soto'
        assert response.data.company == 'CloudBees'
        assert response.data.twitter == '@alexsotob'
        assert response.data.talks.size() == 2
        assert response.data.talks.title == TALK_TITLE
    }

    @Test
    public void postSpeaker_XMLSpeakerProvided_ShouldMirrorPostedSpeaker() {
        def body = {
            speaker(
                    firstName: 'Alex',
                    lastName: 'Soto',
                    company: 'CloudBees',
                    twitter: '@alexsotob'
            )
        }

        def response = http.post(
                path:'/',
                contentType: 'text/xml',
                body: body
        )

        assert response.status == 201
        println "Success: ${response.statusLine}"

        assert response.data instanceof GPathResult
        assert response.data.@firstName == 'Alex'
        assert response.data.@lastName == 'Soto'
        assert response.data.@company == 'CloudBees'
        assert response.data.@twitter == '@alexsotob'
    }

}
