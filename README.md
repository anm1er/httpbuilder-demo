# HTTPBuilder Demo

A first taste of [HTTPBuilder], HTTP client for Groovy with a number of convenience mechanisms
built on top of Apache HTTPClient.


[HTTPBuilder]: https://github.com/jgritman/httpbuilder
